#!/usr/bin/env python2.6
from os.path import expanduser
import os
import sys
import errno
import argparse
import socket
import pycpanel
import subprocess

# Expands the current user's "/home/username/.accesshash
# file if they have it
# Support should be running this with sudo
# so it should default to /root/.accesshash

access_hash = expanduser("~") + "/" + ".accesshash"

# We need this for the hostname the script is running on
serverhostname = socket.gethostname()

sudo_access = True

if sudo_access is True:
    if os.getuid() is not 0:
        print "Sorry, this script cannot be run without\
                sudo or root access at this time"
        exit()
else:
    pass


# Initiates the arguments
parser = argparse.ArgumentParser()
usergroup = parser.add_mutually_exclusive_group(required=True)
# Username or domain name but not both
usergroup.add_argument('-u', '--username', action='store', help='Username')
usergroup.add_argument('-d', '--domain', action='store', help='Domainname')
# User must choose something from the action group too
actiongroup = parser.add_mutually_exclusive_group(required=True)
actiongroup.add_argument('-i', '--info',
                         action='store_true', help='User Information')
actiongroup.add_argument('-s', '--ssh',
                         action='store_true', help='SSH as user')

usergroup = parser.parse_args()

if (usergroup.username == "root"):
    print "Yes we thought of that. Thank you for trying"
    exit()


# This opens up root's accesshash file for authentication
try:
    with open(access_hash, 'rU') as cpanel_hash:
        hash_data = cpanel_hash.read()
except IOError:
    print "File %s does not exist, did you start with sudo?\
            You are on server %s " % (access_hash, serverhostname)
    exit()
    # You need to generate a new accesshash under whm > remote clusters
    # But you shouldn't do this without warning the bizdev/billing
    # team first because whmcs relies on it.


# Connect to the server
server = pycpanel.conn(hostname='localhost', username='root', hash=hash_data)


def get_user_information(username, domainname):

    username = usergroup.user
    domainname = usergroup.domain

    if usergroup.username:
        params = {'searchtype': 'user',
                  'search': username,
                  'want': 'user,suspended,plan,domain,ip,diskused', }
    elif usergroup.domain:
        params = {'searchtype': 'domain',
                  'search': domainname,
                  'want': 'user,suspended,plan,domain,ip,diskused', }
    findacct = server.api('listaccts', params=params)

    print "Username: " + findacct['acct'][0]['user']
    print "Domainname: " + findacct['acct'][0]['domain']
    if findacct['acct'][0]['suspended'] is 0:
        print "User not suspended"
    else:
        print "User is suspended"
    print "Plan: " + findacct['acct'][0]['plan']
    print "IP Address: " + findacct['acct'][0]['ip']
    print "Disk Used: " + findacct['acct'][0]['diskused']


#####
# ssh stuff, basically built for an automatic ssh authentication mech
####

def ssh_gen_key(username):
    comment = "This is a temporary key used by the support team, it may\
        be destroyed at any time"
    params = {'user': username,
              'name': 'supportsshkey',
              'bits': '2048',
              'comment': comment, }

    server.cpanel_api('SSH', 'genkey', username,  params=params)


def ssh_list_key(username, pub=False):
    if pub:
        keyname = 'supportsshkey.pub'
        keytype = 'pub'
    else:
        keyname = 'supportsshkey'
        keytype = 'priv'
    params = {'keys': keyname,
              'type': keytype, }
    server.cpanel_api('SSH', 'listkeys', username, params=params)


def ssh_fetch_key(username, copy=False):
    params = {'name': 'supportsshkey',
              'pub': '0', }
    if copy:
        fetchkeys = server.cpanel_api('SSH',
                                      'fetchkey',
                                      username,
                                      params=params)
# This is for when we want to copy the
# file to the home directory of the
# technician
        private_key = fetchkeys[0]['key']
        return private_key
    else:
        server.cpanel_api('SSH', 'fetchkey', username, params=params)


def ssh_del_key(username, pub=True):
    if pub:
        name = 'supportsshkey.pub'
        pub = '1'
    else:
        name = 'supportsshkey'
        pub = '0'
    params = {'name': name,
              'pub': pub, }
    server.cpanel_api('SSH', 'delkey', username, params=params)


def ssh_auth_key(username, auth=False):
    if auth is True:
        authaction = 'authorize'
    else:
        authaction = 'deauthorize'
    params = {'action': authaction,
              'key': 'supportsshkey.pub', }
    server.cpanel_api('SSH', 'authkey', username, params=params)


def ssh_copy_private(sudo_username):
    sudouid = int(os.getenv('SUDO_UID'))
    sudogid = int(os.getenv('SUDO_GID'))
    privatekeyname = "/.ssh/mysupport.private"
    sudouserpath = expanduser(os.getenv('SUDO_USER'))
    local_key_file = "/home/" + sudouserpath + privatekeyname
    # Users without a ~/.ssh/ directory? Create it for em!
    try:
        os.makedirs("/home/" + sudouserpath + "/.ssh/")
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
    remote_ssh_key = ssh_fetch_key(usergroup.username, copy=True)
    with open(local_key_file, 'w+') as local_key:
        local_key.write(remote_ssh_key)
    os.chmod(local_key_file, 0600)
    os.chown("/home/" + sudouserpath + "/.ssh/", sudouid, sudogid)
    os.chown(local_key_file, sudouid, sudogid)


def ssh_as_user(username):
    privatekeyname = "/.ssh/mysupport.private"
    sudouser = os.getenv('SUDO_USER')
    sudouserpath = expanduser()
    local_key_file = "/home/" + sudouserpath + privatekeyname
    ssh_gen_key(usergroup.username)
    ssh_auth_key(usergroup.username, auth=True)
    ssh_copy_private(sudouser)
    username = usergroup.username
    ssh_cmd = "ssh -i " + local_key_file + " -l " + username + " localhost"
    try:
        sshproc = subprocess.call(ssh_cmd.split())
        if sshproc < 0:
            print >>sys.stderr, "Child was terminated by signal", -sshproc
            ssh_auth_key(usergroup.username, auth=False)
            ssh_del_key(usergroup.username, pub=True)
            ssh_del_key(usergroup.username, pub=False)
        else:
            print >>sys.stderr, "Child returned", -sshproc
            ssh_auth_key(usergroup.username, auth=False)
            ssh_del_key(usergroup.username, pub=True)
            ssh_del_key(usergroup.username, pub=False)
    except OSError as e:
        print >>sys.stderr, "Execution failed:", e

if (usergroup.info and usergroup.username):
    get_user_information(usergroup.username)
elif (usergroup.info and usergroup.domain):
    get_user_information(usergroup.domain)
elif (usergroup.ssh and usergroup.username):
    ssh_as_user(usergroup.username)
else:
    print "Sorry I don't know what to do"
